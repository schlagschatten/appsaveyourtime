package com.example.awesomeapp


import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.*
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.awesomeapp.util.PrefUtil
import com.example.awesomeapp.util.PrefUtil.Companion.getPreviousTimerLengthSeconds
import com.geeksforgeeks.myfirstkotlinapp.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_timer.*
import nl.dionsegijn.konfetti.KonfettiView
import nl.dionsegijn.konfetti.models.Shape
import nl.dionsegijn.konfetti.models.Size
import java.util.*
import com.example.awesomeapp.util.PrefUtil.Companion as PrefUtil1


@Suppress("UNUSED_ANONYMOUS_PARAMETER")
class MainActivity : AppCompatActivity() {


    companion object {
        fun setAlarm(context: Context, nowSeconds: Long, secondsRemaining: Long): Long {
            val wakeUpTime = (nowSeconds + secondsRemaining) * 1000
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(context, TimerExpiredReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, wakeUpTime, pendingIntent)
            PrefUtil.setAlarmSetTime(nowSeconds, context)
            return wakeUpTime
        }

        fun removeAlarm(context: Context) {
            val intent = Intent(context, TimerExpiredReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)
            PrefUtil.setAlarmSetTime(0, context)
        }

        val nowSeconds: Long
            get() = Calendar.getInstance().timeInMillis / 1000
    }

    enum class TimerState {
        Stopped, Paused, Running
    }

    private lateinit var timer: CountDownTimer
    private var timerLengthSeconds = 0L
    private var timerState = TimerState.Stopped
    private var secondsRemaining = 0L
    private var pandaIsHappy = false;


    private var index = 0

    override fun onResume() {
        pandaIsHappy = true;
        if (panda_smile.visibility == View.INVISIBLE && main_activity_text1.visibility == View.INVISIBLE) {
            panda_sad.visibility = View.VISIBLE;
            main_activity_text2.visibility = View.VISIBLE;
        }

        super.onResume()



        initTimer()
        removeAlarm(this)
        //TODO:  hide notification
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStop() {
        super.onStop()

        if (timerState == TimerState.Running) {
            timer.cancel()
            val wakeUpTime = setAlarm(this, nowSeconds, secondsRemaining)

            if (pandaIsHappy) {
                panda_smile.visibility = View.INVISIBLE;
                main_activity_text1.visibility = View.INVISIBLE;

            }
            //TODO: show notification
        } else if (timerState == TimerState.Paused) {

            //Todo: show notification
        }
        PrefUtil1.setPreviousTimerLengthSeconds(timerLengthSeconds, this)
        PrefUtil1.setSecondsRemaining(secondsRemaining, this)
        PrefUtil1.setTimerState(timerState, this)
    }

    private fun initTimer() {
        timerState = PrefUtil1.getTimerState(this)

        if (timerState == TimerState.Stopped){
            setNewTimerLength()
        }
        else
            setPreviousTimerLength()

        secondsRemaining = if (timerState == TimerState.Running || timerState == TimerState.Paused)
            PrefUtil1.getSecondsRemaining(this)
        else {
            timerLengthSeconds
        }

        if (secondsRemaining <= 0) {
            onTimerFinished()
            konfeti()
        }
        else if (timerState == TimerState.Running)
            timerState = TimerState.Paused

        updateButtons()
        updateCountdownUI()
    }

    private fun konfeti(){
        viewKonfetti!!.build()
            .addColors(
                Color.YELLOW,
                Color.GREEN,
                Color.MAGENTA
            )
            .setDirection(0.0, 359.0)
            .setSpeed(1f, 5f)
            .setFadeOutEnabled(true)
            .setTimeToLive(2000L)
            .addShapes(Shape.Square, Shape.Circle)
            .addSizes(Size(12, 5F))
            .setPosition(-50f, viewKonfetti!!.width + 50f, -50f, -50f)
            .streamFor(300, 5000L)
    }

    private fun onTimerFinished() {
        timerState = TimerState.Stopped


        setNewTimerLength()

        progress_countdown.progress = 0

        PrefUtil1.setSecondsRemaining(timerLengthSeconds, this)
        secondsRemaining = timerLengthSeconds

        updateButtons()
        updateCountdownUI()
    }

    private fun startTimer() {
        timerState = TimerState.Running

        timer = object : CountDownTimer(secondsRemaining * 1000, 1000) {
            override fun onFinish() {
                onTimerFinished()
                konfeti()
            }

            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished / 1000
                updateCountdownUI()
            }
        }.start()
    }

    private fun setNewTimerLength() {
        val lengthInMinutes = PrefUtil1.getTimerLength(this)
        timerLengthSeconds = (lengthInMinutes * 60L)
        progress_countdown.max = timerLengthSeconds.toInt()
        progress_countdown.progress=0;
    }

    private fun setPreviousTimerLength() {
        timerLengthSeconds = getPreviousTimerLengthSeconds(this)
        progress_countdown.max = timerLengthSeconds.toInt()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_timer, menu)
        return true
    }

    private fun updateCountdownUI() {
        val minutesUntilFinished = secondsRemaining / 60
        val secondsInMinuteUntilFinished = secondsRemaining - minutesUntilFinished * 60
        val secondsStr = secondsInMinuteUntilFinished.toString()
        textView_countdown.text =
            "$minutesUntilFinished:${if (secondsStr.length == 2) secondsStr else "0" + secondsStr}"
        progress_countdown.progress = (timerLengthSeconds - secondsRemaining).toInt()
    }

    private fun updateButtons() {
        when (timerState) {
            TimerState.Running -> {
                fab_start.isEnabled = false
                fab_pause.isEnabled = true
                fab_stop.isEnabled = true
            }
            TimerState.Stopped -> {
                fab_start.isEnabled = true
                fab_pause.isEnabled = false
                fab_stop.isEnabled = false
            }
            TimerState.Paused -> {
                fab_start.isEnabled = true
                fab_pause.isEnabled = false
                fab_stop.isEnabled = true
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                pandaIsHappy = false;
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        PrefUtil1.setSecondsRemaining(60,this)
        PrefUtil1.setPreviousTimerLengthSeconds(60, this)
        PrefUtil1.setAlarmSetTime(60, this)
        PrefUtil1.setTimerLength(1, this)
        setContentView(R.layout.activity_main)
        supportActionBar?.setIcon(R.drawable.ic_timer)
        supportActionBar?.title = "Save your time"


        val fab_stop =
           findViewById<com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton>(
               R.id.fab_stop)

        fab_stop.setOnClickListener { v ->
            timer.cancel()
            onTimerFinished()
            panda_smile.visibility = View.VISIBLE;
            panda_sad.visibility = View.INVISIBLE;
            main_activity_text1.visibility =  View.VISIBLE;
            main_activity_text2.visibility =  View.INVISIBLE;

        }

        fab_start.setOnClickListener { v ->
            startTimer()
            timerState = TimerState.Running
            updateButtons()
        }

        fab_pause.setOnClickListener { v ->
            timer.cancel()
            timerState = TimerState.Paused
            updateButtons()
        }

    }

}

